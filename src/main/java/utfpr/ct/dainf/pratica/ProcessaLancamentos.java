package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Linguagem Java
 * @author
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
        String linha;
        if((linha = reader.readLine()) != null){
            return linha;
        }
        else{
            return null;
        }
    }
    
    private Lancamento processaLinha(String linha) throws ParseException {
        Lancamento lancamento = new Lancamento(null,null,null,null);
        Scanner scanner;
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        lancamento.setConta(Integer.parseInt(linha.substring(0,6)));
        lancamento.setData((Date)formatter.parse(linha.substring(12,14) + "/" + linha.substring(10, 12) + "/" + linha.subSequence(6, 10)));
        linha = linha.substring(14);
        scanner = new Scanner(linha);
        lancamento.setDescricao(scanner.next() + " " + scanner.next());
        lancamento.setValor(scanner.nextDouble());
        return lancamento;
    }
    
    private Lancamento getNextLancamento() throws IOException, ParseException {
        String linha;
        if((linha = getNextLine()) != null){
            return processaLinha(linha);
        }
        else{
            return null;
        }
    }
    
    public List<Lancamento> getLancamentos() throws IOException, ParseException {
        List<Lancamento> lista = new ArrayList<>();
        Lancamento lancamento;
        LancamentoComparator comparador = new LancamentoComparator();
        while((lancamento = getNextLancamento()) != null){
            lista.add(lancamento);
        }
        sort(lista,comparador);
        this.reader.close();
        return lista;
    }
    
}
