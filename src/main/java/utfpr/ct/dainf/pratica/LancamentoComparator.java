package utfpr.ct.dainf.pratica;

import java.util.Comparator;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparator<Lancamento> {
    
    
    
    @Override
    public int compare(Lancamento lancamento1,Lancamento lancamento2){
        if(lancamento1.getConta().equals(lancamento2.getConta())){
            return lancamento1.getData().compareTo(lancamento2.getData());
        }
        else{
            return lancamento1.getConta().compareTo(lancamento2.getConta());
        }
    }
}
