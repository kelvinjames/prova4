
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.LancamentoComparator;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
// caminho : /home/todos/alunos/ct/a1986813/NetBeansProjects/java-pratica-arquivo-banco/src/main/resources/lancamentos.txt
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
        Scanner scanner;
        String caminho;
        int indicadora = 0;
        System.out.println("digite o caminho do arquivo");
        scanner = new Scanner(System.in);
        caminho = scanner.next();
        ProcessaLancamentos processa = new ProcessaLancamentos(caminho);
        List<Lancamento> lista = processa.getLancamentos();
        int conta = 1;
        LancamentoComparator comparador = new LancamentoComparator();
        for(Lancamento teste: lista){
            System.out.println(teste.toString());
        }
        while(conta != 0){
            System.out.println("digite o número da conta");
            scanner = new Scanner(System.in);
            if(scanner.hasNextInt()){
                conta = scanner.nextInt();
                if(conta == 0){
                    break;
                }
            }
            else{
                System.out.println("Por favor, informe um valor numérico");
                continue;
            }
            for(Lancamento aux : lista){
                if(aux.getConta().equals(conta)){
                    exibeLancamentosConta(lista,conta);
                    indicadora = 1;
                    break;
                }
            }
            if(indicadora == 0){
                System.out.println("Conta inexistente");
            }
            indicadora = 0;
        }
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        List<Lancamento> listaConta = new ArrayList<>();
        for(Lancamento aux : lancamentos){
            if(aux.getConta().equals(conta)){
                listaConta.add(aux);
            }
        }
        for(Lancamento aux1 : listaConta){
            System.out.println(aux1.toString());
        }
    }
 
}